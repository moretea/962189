class ProductChangeNotification
  belongs_to :product
  belongs_to :user
end