class Product
  has_many :notifications, :class => "ProductChangeNotification"
  has_many :notifees, :through => "notifications", :source => :user

  after_save :send_notifications

protected
  def send_notifications
    notifees.each do |notifee|
      NotificationMailer.product_change(self, notifee).deliver!
    end
  end
end