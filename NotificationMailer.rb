# See http://edgeguides.rubyonrails.org/action_mailer_basics.html for action mailer usage
class NotificationMailer < ActionMailer::Base
  default :from => "notifications@example.com"
 
  def product_change(product, user)
    @product = product
    mail(:to => user.email, :subject => "Welcome to My Awesome Site")
  end
 end